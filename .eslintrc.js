module.exports = {
  root: true,
  parser: 'babel-eslint',
  plugins: ['import', 'jsx-a11y', 'react'],
  extends: ['react-app'],

  env: {
    browser: true,
    commonjs: true,
    es6: true,
    jest: true,
    node: true,
  },

  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },

  rules: {
    'function-paren-newline': 'off',
    'import/no-extraneous-dependencies': 'off',
    'jsx-a11y/label-has-for': [
      2,
      {
        components: ['Label'],
        required: {
          every: ['id'],
        },
        allowChildren: false,
      },
    ],
    'jsx-a11y/href-no-hash': 'off',
    'no-case-declarations': 0,
    'object-curly-newline': 'off',
    'import/no-dynamic-require': 'warn',
    'import/newline-after-import': 0,
    'global-require': 'warn',
    'no-mixed-operators': ['error', { allowSamePrecedence: true }],
    'react/button-has-type': 0,
    'react/jsx-filename-extension': 0,
    'no-param-reassign': 0,
    'react/boolean-prop-naming': [
      1,
      { rule: `^(active|checked|collapsed|dirty|disabled)$|^(is|has)[A-Z]([A-Za-z0-9]?)+` },
    ],
    'react-hooks/exhaustive-deps': 0,
    'no-console': ['warn', { allow: ['error'] }],
    'import/prefer-default-export': 0,
    'react/prefer-stateless-function': 'warn',
  },
};
