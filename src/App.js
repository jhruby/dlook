import React, { Suspense } from 'react';
import { ThemeProvider } from 'styled-components';
import theme from '@rebass/preset';
import { SWRConfig } from 'swr';
import { Router, Redirect } from '@reach/router';

import { swrConfig } from 'utils/FetchUtil';
import ArticleList from 'features/Articles/All';
import ArticleDetail from 'features/Articles/Detail';
import ArticleSearch from 'features/Articles/Search';
import ErrorBoundary from 'components/ErrorBoundary';
import GlobalStyles from 'styles';
import Navbar from 'components/Navbar';
import Routes from 'enums/Routes';
import Spinner from 'components/Spinner';

const Route = ({ children }) => children;

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <SWRConfig value={swrConfig}>
        <GlobalStyles />
        <Navbar />

        <ErrorBoundary>
          <Suspense fallback={<Spinner />}>
            <Router basepath={process.env.PUBLIC_URL}>
              <Route path={Routes.ARTICLES.BASE}>
                <ArticleList default />
                <ArticleDetail path={Routes.ARTICLES.DETAIL} />
                <ArticleSearch path={Routes.ARTICLES.SEARCH} />
              </Route>
              <Redirect from={Routes.ROOT} to={Routes.ARTICLES.BASE} noThrow />
            </Router>
          </Suspense>
        </ErrorBoundary>
      </SWRConfig>
    </ThemeProvider>
  );
};

export default App;
