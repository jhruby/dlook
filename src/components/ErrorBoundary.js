import React from 'react';
import { Heading } from 'rebass/styled-components';

class ErrorBoundary extends React.Component {
  state = { hasError: false };

  static getDerivedStateFromError() {
    return { hasError: true };
  }

  render() {
    if (this.state.hasError) {
      return (
        <Heading as="h1" fontSize={5} textAlign="center" mt={100}>
          Something went wrong!
        </Heading>
      );
    }
    return this.props.children;
  }
}

export default ErrorBoundary;
