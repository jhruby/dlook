import React from 'react';
import PropTypes from 'prop-types';
import { Link as ReachLink } from '@reach/router';

const Link = ({ to = '', children, absolute, ...props }) => {
  const relativeTo = !absolute && to.startsWith('/') ? process.env.PUBLIC_URL + to : to;
  return (
    <ReachLink {...props} to={relativeTo}>
      {children}
    </ReachLink>
  );
};

Link.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  absolute: PropTypes.bool, //eslint-disable-line react/boolean-prop-naming
};

Link.defaultProps = {
  absolute: false,
};

export default Link;
