import React from 'react';
import { Flex, Text, Box, Link } from 'rebass/styled-components';

const Navbar = () => (
  <Flex p={3} color="white" bg="black" alignItems="center" sx={{ position: 'sticky', top: 0 }}>
    <Text p={2} fontWeight="bold">
      DLOOK
    </Text>
    <Box mx="auto" />
    <Link variant="nav" href="#!">
      Profile
    </Link>
  </Flex>
);

export default Navbar;
