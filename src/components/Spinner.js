import React from 'react';
import { Flex } from 'rebass/styled-components';
import styled, { keyframes } from 'styled-components';

const loaderScale = keyframes`
  0% {
    transform: scale(0);
    opacity: 0; }
  50% {
    opacity: 1; }
  100% {
    transform: scale(1);
    opacity: 0; }
`;

const Loader = styled.div`
  width: 56px;
  height: 56px;
  border: 4px solid transparent;
  border-radius: 50%;
  position: relative;
  top: 50%;
  margin: -28px auto 0;

  &::before {
    content: '';
    border: 4px solid rgba(0, 82, 236, 0.5);
    border-radius: 50%;
    width: 67.2px;
    height: 67.2px;
    position: absolute;
    top: -9.6px;
    left: -9.6px;
    animation: ${loaderScale} 1s ease-out infinite;
    animation-delay: 0.5s;
    opacity: 0;
  }

  &::after {
    content: '';
    border: 4px solid #0052ec;
    border-radius: 50%;
    width: 56px;
    height: 56px;
    position: absolute;
    top: -4px;
    left: -4px;
    animation: ${loaderScale} 1s ease-out infinite;
  }
`;

const Spinner = () => (
  <Flex mt={240} justifyContent="center" alignItems="center" sx={{ position: 'relative' }}>
    <Loader />
  </Flex>
);

export default Spinner;
