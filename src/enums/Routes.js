export default {
  ROOT: '/',
  ARTICLES: {
    BASE: '/articles',
    DETAIL: ':articleId',
    SEARCH: 'search',
  },
};
