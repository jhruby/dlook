import React from 'react';
import useSWR from 'swr';

import { ArticleList, ArticleListSlice } from 'features/Articles/List';
import * as ArticleService from 'services/Articles';

const ArticleGetAllQuery = ({ offset, withSWR }) => {
  const { data } = withSWR(
    useSWR(ArticleService.getAllArticles({ page: offset }), { suspense: false }),
  );
  return (
    <ArticleListSlice
      hasError={data && data.errors}
      isLoading={!data || !data.articles}
      articles={data && data.articles}
    />
  );
};

const AllArticles = () => (
  <ArticleList title="Nejnovější analýzy" pageKey="all-articles" pageFn={ArticleGetAllQuery} />
);

export default AllArticles;
