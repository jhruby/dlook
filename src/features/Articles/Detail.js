import React from 'react';
import PropTypes from 'prop-types';
import useSWR from 'swr';
import { Card, Box, Flex, Image, Heading, Text } from 'rebass/styled-components';

import * as ArticleService from 'services/Articles';
import Link from 'components/Link';
import properties from 'properties';

const ArticleDetail = ({ articleId }) => {
  const { data: article } = useSWR(ArticleService.getArticleById(articleId));
  const { title, subtitle, image, introduction, body } = article;

  return (
    <Card
      width={[1, 1, 1, 800]}
      mx="auto"
      my={[0, 0, 0, 3]}
      p={0}
      sx={{ borderRadius: [0, 0, 0, 4], overflow: 'hidden' }}
    >
      <Flex as={Link} to=".." m={3}>
        Zpátky na přehled
      </Flex>

      <Image src={properties.WIDE_IMAGE_URL + image} height={240} sx={{ objectFit: 'cover' }} />

      <Box p={3}>
        <Heading as="h1" mb={1}>
          {title}
        </Heading>
        <Heading as="h2" fontSize={3} mb={3}>
          {subtitle}
        </Heading>
        <Text fontSize={2} fontWeight="bold" mb={3}>
          {introduction}
        </Text>
        <Box dangerouslySetInnerHTML={{ __html: body }} />
      </Box>
    </Card>
  );
};

ArticleDetail.propTypes = { articleId: PropTypes.string };
ArticleDetail.defaultProps = { articleId: null };

export default ArticleDetail;
