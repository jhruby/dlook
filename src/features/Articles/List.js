import React, { useEffect } from 'react';
import { useSWRPages, mutate } from 'swr';
import { Flex, Heading, Button, Box } from 'rebass/styled-components';

import * as ArticleService from 'services/Articles';
import ArticleListItem from 'features/Articles/ListItem';
import Spinner from 'components/Spinner';

export const ArticleList = ({ title, pageKey, pageFn }) => {
  const { pages, pageSWRs, isLoadingMore, isReachingEnd, loadMore } = useSWRPages(
    pageKey,
    pageFn,
    ({ data }) => (data.page * data.pageSize < data.totalCount ? data.page + 1 : null),
    [],
  );
  const isInitialLoad = !pageSWRs.length || !pageSWRs[0].data;

  return (
    <Flex flexDirection="column" pb={4} alignItems="center" width={[1, 1, 1, 800]} mx="auto">
      <Heading as="h1" sx={{ px: 4, py: 3 }}>
        {title}
      </Heading>

      <Box as="ul" mb={3}>
        {pages}
      </Box>

      {isInitialLoad && <Spinner />}

      {!isReachingEnd && !isInitialLoad && (
        <Button onClick={loadMore} variant="outline" sx={{ cursor: 'pointer', mx: 4 }}>
          {isLoadingMore ? 'Loading...' : 'Load more'}
        </Button>
      )}
    </Flex>
  );
};

const saveArticle = article =>
  mutate(ArticleService.getArticleById(article.friendlyUrl), article, false);

export const ArticleListSlice = ({ hasError, isLoading, articles }) => {
  useEffect(() => {
    if (articles) {
      articles.forEach(saveArticle);
    }
  }, [articles]);

  if (hasError) return 'Error loading articles!';
  if (isLoading) return null;
  if (!articles.length) return 'No articles found';

  return articles.map(article => <ArticleListItem key={article.friendlyUrl} article={article} />);
};
