import React from 'react';
import PropTypes from 'prop-types';
import { Card, Heading, Text, Image, Flex, Box } from 'rebass/styled-components';

import Link from 'components/Link';
import properties from 'properties';

const ArticleListItem = ({ article: { friendlyUrl, title, subtitle, image, introduction } }) => (
  <Card as="li" sx={{ p: 4 }}>
    <Flex as={Link} to={friendlyUrl} sx={{ flexDirection: ['column', 'row'] }}>
      <Image
        src={properties.SQUARE_IMAGE_URL + image}
        minWidth={160}
        height={160}
        mr={[0, 4]}
        mb={[3, 0]}
        sx={{ borderRadius: 4, objectFit: 'cover' }}
      />
      <Box>
        <Heading as="h2" mb={1}>
          {title}
        </Heading>
        <Heading as="h3" fontSize={3} mb={3}>
          {subtitle}
        </Heading>
        <Text fontSize={2}>{introduction}</Text>
      </Box>
    </Flex>
  </Card>
);

ArticleListItem.propTypes = {
  article: PropTypes.shape({
    friendlyUrl: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    introduction: PropTypes.string.isRequired,
  }).isRequired,
};

export default ArticleListItem;
