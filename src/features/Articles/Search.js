import React from 'react';
import useSWR from 'swr';

import { ArticleList, ArticleListSlice } from 'features/Articles/List';
import * as ArticleService from 'services/Articles';

const ArticleSearchQuery = ({ offset, withSWR, searchExpression }) => {
  const { data } = withSWR(
    useSWR(ArticleService.searchArticles({ page: offset, searchExpression }), { suspense: false }),
  );
  return (
    <ArticleListSlice
      hasError={data && data.errors}
      isLoading={!data || !data.articles}
      articles={data && data.articles}
    />
  );
};

const AllArticles = () => {
  const queryParams = new URLSearchParams(window.location.search);
  const searchExpression = queryParams.get('searchExpression');

  return (
    <ArticleList
      title={`Vyhledávání: ${searchExpression}`}
      pageKey="search-articles"
      pageFn={props => <ArticleSearchQuery {...props} searchExpression={searchExpression} />}
    />
  );
};

export default AllArticles;
