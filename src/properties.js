export default {
  ARTICLES_PER_PAGE: 20,

  SQUARE_IMAGE_URL: 'https://dlook.deloitte.cz/Upload/Images/square/',
  WIDE_IMAGE_URL: 'https://dlook.deloitte.cz/Upload/Images/wide/',
  API_BASE_URL: 'https://dlookapi.azurewebsites.net',
};
