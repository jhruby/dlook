import { makeQueryURL } from 'utils/FetchUtil';
import properties from 'properties';

export const getAllArticles = ({ page, pageSize = properties.ARTICLES_PER_PAGE } = {}) =>
  makeQueryURL('/article/list', {
    page: page || 1,
    pageSize,
  });

export const getArticleById = articleUrl =>
  makeQueryURL('/article/detail', {
    articleUrl,
  });

export const searchArticles = ({
  page,
  pageSize = properties.ARTICLES_PER_PAGE,
  searchExpression,
}) =>
  makeQueryURL('/article/search', {
    page: page || 1,
    pageSize,
    searchExpression,
  });
