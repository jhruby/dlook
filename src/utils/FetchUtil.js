import properties from 'properties';

const makeQuery = params =>
  Object.entries(params)
    .map(([key, value]) => [key, value].map(encodeURIComponent).join('='))
    .join('&');

export const makeQueryURL = (url, params) => [url, makeQuery(params)].join('?');

export const fetcher = (url, options) =>
  fetch([properties.API_BASE_URL, url].join('/'), {
    ...options,
    headers: {
      ...(options = {}).headers,
      'Accept-Language': 'cs',
    },
  }).then(res => res.json());

const postFetcher = (url, body) =>
  fetcher(url, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: { 'Content-Type': 'application/json' },
  });

fetcher.GET = fetcher;
fetcher.POST = postFetcher;

export const swrConfig = {
  fetcher: fetcher.GET,
  suspense: true,
  revalidateOnFocus: false,
  dedupingInterval: Math.pow(2, 31) - 1, // max value for setTimeout
};
